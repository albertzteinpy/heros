# -*- coding: utf-8 -*-

from django.conf.urls import url,patterns
from . import views as DViews 


urlpatterns = [
   url(r'^home/$', DViews.Index),
   url(r'^consultando/$', DViews.Consulta),
   url(r'^recipe/$', DViews.Recipe,name='recipe'),
   #url(r'^singon/$', MViews.Son),
]

