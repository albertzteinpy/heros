# -*- coding: utf-8 -*-
"""Proyect url's."""

from django.conf.urls import patterns, url

urlpatterns = patterns(
    'hospital.views',
    '''
    url(r'^manager/login/$', 'LoginView', name='login'),
    url(r'^manager/logout/$', 'LogoutView', name='logout'),
    url(r'^manager/$', 'index', name='index'),

    url(r'^manager/profile/$', 'ProfileView', name='profile'),
    url(r'^manager/pass/change/$', 'PassChangeView', name='password'),
    url(r'^manager/pass/retrieve/$', 'PassRetrieveView', name='pass_retrieve'),


    url(r'^admin/logout/$', 'LogoutView', name='logout'),
    '''
)

