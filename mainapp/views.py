# -*- coding: utf-8 -*-
"""
Views to managment companies.

This module is used to the managment the companies in the content manage
system.
"""

from django.http import HttpResponse
from django.shortcuts import redirect, render, render_to_response
from django.template.context import RequestContext
from django.views.generic import View
from django.contrib.auth.models import User,Group
from superhero.modelforms import FormCreator
from django import forms
import simplejson
import datetime
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.hashers import make_password
from django.core.mail import send_mail, BadHeaderError
from superhero.settings import EMAIL_HOST
from django.core.mail import send_mail, BadHeaderError
from django.core.exceptions import ValidationError


from django.core.mail import send_mail


class Index(View):
    """Delete company."""
    template = 'mainapp/index.html'

    def get(self, request):

        logout_pass = request.GET.get('logout',None)
        if logout_pass:
            logout(request)


        if request.user.is_active:
            response = redirect("/doctor/home/")
            return response
        context = {}
        forma = FormCreator()
        widgets = {
            'password':forms.PasswordInput(),
        }

        forma = forma.advanced_form_to_model(modelo=User,
                                             fields=['username','password','first_name','last_name','email'],
                                             widgets = widgets
                                             )
        for x,y in forma.base_fields.iteritems():
            (y.widget.attrs.update({'class':'form-control'}))

        context['forma'] = forma
        response = render(request, self.template, context)
        return response

Index = Index.as_view()



class Sin(View):

    def post(self,request):
        data = request.POST.copy()
        user = authenticate(username=data['usuarius'], password=data['passo'])
        response = {}
        try:
            group = user.groups.values()[0]['name']
        except:
            user = None
            group = None

        if user is not None:
            login(request, user)
            response['saved'] = 'ok'
            response['datos'] = {'msg':u'guardao con éxito','liga':'/%s/home/'%(group)}
            response['callback'] = 'reg_success'
        else:
            response['msg'] = 'la información no es correcta, verifique por favor'
            response['errors'] = {'usuarius':'Usuario no es correcto'}

        return HttpResponse(simplejson.dumps(response))


Sin = Sin.as_view()


class Son(View):

    def post(self,request):
        data = request.POST.copy()
        data['date_joined'] = datetime.datetime.now()
        instanced = None
        response = {}
        forma = FormCreator()
        forma = forma.advanced_form_to_model(modelo=User,
                                             excludes = []
                                             )

        forma.base_fields['email'].required=True
        forma.base_fields['groups'].required=True
        grupo = data['groups']
        ges = Group.objects.get(name='%s'%data['groups'])
        data['groups'] = (ges.pk)
        data['is_active']=True
        data['is_staff']=True
        pax = data['password']
        data['password']=make_password(data['password'])
        forma = forma(data,instance=instanced)

        if forma.is_valid():
            saved = forma.save()
            if saved:
                usery = authenticate(username=data['username'], password=pax)
                login(request, usery)


            response['saved'] = 'ok'
            response['datos'] = {'msg':u'guardao con éxito','liga':'/%s/home/'%(grupo)}
            response['callback'] = 'reg_success'

        else:
            response['msg'] = 'Verifique su información'
            response['errors'] = forma.errors


        returns = HttpResponse(simplejson.dumps(response))
        
        return returns

Son = Son.as_view()


