# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User

# Create your models here.

PATIENT_TYPE = (
				('Pediatrico','Pediatrico'),
				('Adulto','Adulto'),
				('Geriatrico','Geriatrico'),
				('Discapacidad','Discapacidad'),
				('Cronico','Cronico'),
	)


GENRES = (
		('M','M'),
		('F','F'),
	)



STATUS_SCHEDULES = (

	('calendario','calendario'),
	('cancelada','cancelada'),
	('en espera','en espera'),
	('atendiendo','atendiendo'),
	('atendida','atendida'),
	)


class Patient(models.Model):

	full_name = models.CharField(u'Nombre Completo',max_length=200)
	patient_yb = models.CharField(u'Año naciemiento',max_length=5)
	current_type = models.CharField(u'Tipo',max_length=100,choices=PATIENT_TYPE)
	genre = models.CharField(u'Genero',max_length=5,choices=GENRES)
	tutor = models.ForeignKey(User,verbose_name='Responsable( en caso de ser Menor)',blank=True,null=True,related_name='tutores')
	doctorpk = models.ManyToManyField(User,related_name='doctores')

class Scheduler(models.Model):
	patientpksch = models.ForeignKey(Patient)
	date_date = models.DateTimeField(u'Fecha de Consulta')
	status = models.CharField(u'situacion',max_length=100,choices=STATUS_SCHEDULES)

class Metrics(models.Model):
	datedata = models.ForeignKey(Scheduler)
	patientpkmtrs = models.ForeignKey(Patient)
	current_date = models.DateTimeField(auto_now_add=True)
	metric_data = models.TextField(u'Metricas')

class Sintoms(models.Model):
	datedata = models.ForeignKey(Scheduler)
	patientpkstm = models.ForeignKey(Patient)
	current_date = models.DateTimeField(auto_now_add=True)
	sintoms_data = models.TextField(u'Metricas')


	

