# -*- coding: utf-8 -*-
"""
Views to managment companies.

This module is used to the managment the companies in the content manage
system.
"""

from django.http import HttpResponse
from django.shortcuts import redirect, render, render_to_response
from django.template.context import RequestContext
from django.views.generic import View
import datetime
from reportlab.pdfgen import canvas
from django.http import HttpResponse

class Index(View):
    """Delete company."""
    template = 'patient/index.html'

    def get(self, request):
        context = {}
        context['today'] = datetime.datetime.now()
        response = render(request, self.template, context)
        return response

Index = Index.as_view()


class Consulta(View):
    template = 'doctor/consultando.html'

    def get(self, request):
        ptky = request.GET.get('ptky',None)
        context = {}
        context['today'] = datetime.datetime.now()
        response = render(request, self.template, context)
        return response

Consulta = Consulta.as_view()


class Recipe(View):

    def  post(self, request):
        data = request.POST.copy()
        # Create the HttpResponse object with the appropriate PDF headers.
        response = HttpResponse(content_type='application/pdf')
        response['Content-Disposition'] = 'attachment; filename="somefilename.pdf"'

        # Create the PDF object, using the response object as its "file."
        #p = canvas.Canvas(response)
        p = canvas.Canvas(response, pagesize='letter')
        # Draw things on the PDF. Here's where the PDF generation happens.
        # See the ReportLab documentation for the full list of functionality.
        p.drawString(1,0, '%s'%(data['diagnostic']))
        p.drawString(100,100, '%s'%(data['medication']))


        # Close the PDF object cleanly, and we're done.
        p.showPage()
        p.save()
        return response
Recipe = Recipe.as_view()
